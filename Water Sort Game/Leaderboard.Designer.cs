﻿
namespace Water_Sort_Game
{
    partial class Leaderboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Leaderboard));
            this.lEasy = new System.Windows.Forms.Label();
            this.lMedium = new System.Windows.Forms.Label();
            this.lHard = new System.Windows.Forms.Label();
            this.lArcade = new System.Windows.Forms.Label();
            this.lbEasy = new System.Windows.Forms.ListBox();
            this.lbMedium = new System.Windows.Forms.ListBox();
            this.lbHard = new System.Windows.Forms.ListBox();
            this.lbArcade = new System.Windows.Forms.ListBox();
            this.bBack = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lEasy
            // 
            this.lEasy.AutoSize = true;
            this.lEasy.BackColor = System.Drawing.Color.Black;
            this.lEasy.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lEasy.Font = new System.Drawing.Font("Showcard Gothic", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lEasy.ForeColor = System.Drawing.Color.White;
            this.lEasy.Location = new System.Drawing.Point(3, 80);
            this.lEasy.Name = "lEasy";
            this.lEasy.Size = new System.Drawing.Size(339, 60);
            this.lEasy.TabIndex = 0;
            this.lEasy.Text = "Easy";
            this.lEasy.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lMedium
            // 
            this.lMedium.AutoSize = true;
            this.lMedium.BackColor = System.Drawing.Color.Black;
            this.lMedium.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lMedium.Font = new System.Drawing.Font("Showcard Gothic", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lMedium.ForeColor = System.Drawing.Color.White;
            this.lMedium.Location = new System.Drawing.Point(3, 347);
            this.lMedium.Name = "lMedium";
            this.lMedium.Size = new System.Drawing.Size(339, 60);
            this.lMedium.TabIndex = 1;
            this.lMedium.Text = "Medium";
            this.lMedium.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lHard
            // 
            this.lHard.AutoSize = true;
            this.lHard.BackColor = System.Drawing.Color.Black;
            this.lHard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lHard.Font = new System.Drawing.Font("Showcard Gothic", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lHard.ForeColor = System.Drawing.Color.White;
            this.lHard.Location = new System.Drawing.Point(348, 0);
            this.lHard.Name = "lHard";
            this.lHard.Size = new System.Drawing.Size(339, 80);
            this.lHard.TabIndex = 2;
            this.lHard.Text = "Hard";
            this.lHard.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lArcade
            // 
            this.lArcade.AutoSize = true;
            this.lArcade.BackColor = System.Drawing.Color.Black;
            this.lArcade.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lArcade.Font = new System.Drawing.Font("Showcard Gothic", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lArcade.ForeColor = System.Drawing.Color.White;
            this.lArcade.Location = new System.Drawing.Point(693, 0);
            this.lArcade.Name = "lArcade";
            this.lArcade.Size = new System.Drawing.Size(352, 80);
            this.lArcade.TabIndex = 3;
            this.lArcade.Text = "Arcade";
            this.lArcade.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbEasy
            // 
            this.lbEasy.BackColor = System.Drawing.Color.Black;
            this.lbEasy.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbEasy.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbEasy.Font = new System.Drawing.Font("Showcard Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbEasy.ForeColor = System.Drawing.Color.White;
            this.lbEasy.FormattingEnabled = true;
            this.lbEasy.ItemHeight = 44;
            this.lbEasy.Location = new System.Drawing.Point(3, 143);
            this.lbEasy.Name = "lbEasy";
            this.lbEasy.Size = new System.Drawing.Size(339, 201);
            this.lbEasy.TabIndex = 4;
            // 
            // lbMedium
            // 
            this.lbMedium.BackColor = System.Drawing.Color.Black;
            this.lbMedium.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbMedium.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbMedium.Font = new System.Drawing.Font("Showcard Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMedium.ForeColor = System.Drawing.Color.White;
            this.lbMedium.FormattingEnabled = true;
            this.lbMedium.ItemHeight = 44;
            this.lbMedium.Location = new System.Drawing.Point(3, 410);
            this.lbMedium.Name = "lbMedium";
            this.lbMedium.Size = new System.Drawing.Size(339, 202);
            this.lbMedium.TabIndex = 5;
            // 
            // lbHard
            // 
            this.lbHard.BackColor = System.Drawing.Color.Black;
            this.lbHard.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbHard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbHard.Font = new System.Drawing.Font("Showcard Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbHard.ForeColor = System.Drawing.Color.White;
            this.lbHard.FormattingEnabled = true;
            this.lbHard.ItemHeight = 44;
            this.lbHard.Location = new System.Drawing.Point(348, 83);
            this.lbHard.Name = "lbHard";
            this.tableLayoutPanel1.SetRowSpan(this.lbHard, 4);
            this.lbHard.Size = new System.Drawing.Size(339, 529);
            this.lbHard.TabIndex = 5;
            // 
            // lbArcade
            // 
            this.lbArcade.BackColor = System.Drawing.Color.Black;
            this.lbArcade.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbArcade.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbArcade.Font = new System.Drawing.Font("Showcard Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbArcade.ForeColor = System.Drawing.Color.White;
            this.lbArcade.FormattingEnabled = true;
            this.lbArcade.ItemHeight = 44;
            this.lbArcade.Location = new System.Drawing.Point(693, 83);
            this.lbArcade.Name = "lbArcade";
            this.tableLayoutPanel1.SetRowSpan(this.lbArcade, 4);
            this.lbArcade.Size = new System.Drawing.Size(352, 529);
            this.lbArcade.TabIndex = 6;
            // 
            // bBack
            // 
            this.bBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.bBack.CausesValidation = false;
            this.bBack.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bBack.FlatAppearance.BorderSize = 0;
            this.bBack.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.bBack.Font = new System.Drawing.Font("Showcard Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bBack.ForeColor = System.Drawing.Color.Transparent;
            this.bBack.Location = new System.Drawing.Point(120, 3);
            this.bBack.Margin = new System.Windows.Forms.Padding(120, 3, 120, 3);
            this.bBack.MaximumSize = new System.Drawing.Size(250, 70);
            this.bBack.Name = "bBack";
            this.bBack.Size = new System.Drawing.Size(105, 70);
            this.bBack.TabIndex = 7;
            this.bBack.TabStop = false;
            this.bBack.Text = "Back";
            this.bBack.UseVisualStyleBackColor = false;
            this.bBack.Click += new System.EventHandler(this.bBack_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34F));
            this.tableLayoutPanel1.Controls.Add(this.bBack, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lbEasy, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lbMedium, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.lbHard, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbArcade, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.lMedium, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lHard, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lEasy, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lArcade, 2, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(58, 34);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1048, 615);
            this.tableLayoutPanel1.TabIndex = 8;
            // 
            // Leaderboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(1178, 678);
            this.ControlBox = false;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1200, 700);
            this.Name = "Leaderboard";
            this.Text = "Water Sort";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lEasy;
        private System.Windows.Forms.Label lMedium;
        private System.Windows.Forms.Label lHard;
        private System.Windows.Forms.Label lArcade;
        private System.Windows.Forms.ListBox lbEasy;
        private System.Windows.Forms.ListBox lbMedium;
        private System.Windows.Forms.ListBox lbHard;
        private System.Windows.Forms.ListBox lbArcade;
        private System.Windows.Forms.Button bBack;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}