﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Water_Sort_Game
{
    public partial class Leaderboard : Form
    {   
        public List<PointData> stats { get; set; }

        public Leaderboard()
        {
            InitializeComponent();
            FormBorderStyle = FormBorderStyle.None;
            WindowState = FormWindowState.Maximized;
            stats = PointData.Load();
            populate();
        }
        private void populate()
        {
            if (stats != null)
            {
                List<PointData> easy = new List<PointData>();
                List<PointData> medium = new List<PointData>();
                List<PointData> hard = new List<PointData>();
                List<PointData> arcade = new List<PointData>();
                foreach (PointData s in stats)
                {
                    switch (s.gamemode)
                    {
                        case 1:
                            easy.Add(s);
                            break;
                        case 2:
                            medium.Add(s);
                            break;
                        case 3:
                            hard.Add(s);
                            break;
                        case 4:
                            arcade.Add(s);
                            break;
                    }
                }
                easy.Sort();
                medium.Sort();
                hard.Sort();
                arcade.Sort();
                foreach (PointData s in easy)
                {
                    lbEasy.Items.Add(s);
                }
                foreach (PointData s in medium)
                {
                    lbMedium.Items.Add(s);
                }
                foreach (PointData s in hard)
                {
                    lbHard.Items.Add(s);
                }
                foreach (PointData s in arcade)
                {
                    lbArcade.Items.Add(s);
                }
            }
        }
        private void bBack_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }
    }
}
