﻿using System;
using System.Drawing;

namespace Water_Sort_Game
{
    public class Square : Shape
    {
        public float Width { get; set; }
        public float Height { get; set; }
        public Square(float Width, float Height, float X, float Y, Color ShapeColor) : base(X, Y, ShapeColor)
        {
            this.Width = Width;
            this.Height = Height;
        }
        public override void Draw(Graphics g)
        {
            Brush b = new SolidBrush(ShapeColor);
            g.FillRectangle(b, X - Width / 2, Y - Height / 2, Width, Height);
            b.Dispose();
        }
        public virtual bool IsHit(float X, float Y)
        {
            return X >= this.X && X <= Width + this.X && Y >= this.Y && Y <= Height + this.Y;
        }
    }
}
