﻿using System.Drawing;

namespace Water_Sort_Game
{
    public class LevelTile : Square
    {
        Title Text { get; set; }
        public int Degree { get; set; }
        Degree Value { get; set; }
        public LevelTile(float Width, float Height, float X, float Y, int Degree) : base(Width, Height, X, Y, Color.Transparent)
        {
            this.Degree = Degree;
            switch (Degree)
            {
                case 1:
                    Text = new Title(X + 29, Y + 200, Color.White, "Easy");
                    break;
                case 2:
                    Text = new Title(X + 15, Y + 200, Color.White, "Medium");
                    break;
                case 3:
                    Text = new Title(X + 25, Y + 200, Color.White, "Hard");
                    break;
                case 4:
                    Text = new Title(X + 16, Y + 200, Color.White, "Arcade");
                    break;
            }
            Value = new Degree(X + 20, Y + 150, Color.Transparent, Degree);
        }

        public override void Draw(Graphics g)
        {
            Text.Draw(g);
            Value.Draw(g);
        }
    }
}
