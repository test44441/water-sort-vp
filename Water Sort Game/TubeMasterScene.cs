﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace Water_Sort_Game
{
    public class TubeMasterScene
    {
        public int TubesAmount { get; set; }
        public Tube[] Tubes;
        public int points { get; set; }
        public int time { get; set; }
        public int TubeSize { get; set; }
        public static Color[] PossibleColors = new Color [] { Color.Red, Color.Green, Color.Blue, Color.LightYellow, Color.Magenta, Color.Cyan, Color.Brown, Color.Pink, Color.Purple, Color.Gray, Color.Gold, Color.White };
        public Color[] SceneColors { get; set; }
        public int Difficulty { get; set; }
        public TubeMasterScene(int Difficulty)
        {
            this.Difficulty = Difficulty;
            GenerateLevel(Difficulty);
            points = 1000;
            time = 0;
        }
        public TubeMasterScene(int points, int time)
        {
            GenerateLevel(3);
            this.points = points;
            this.time = time;
        }
        public void GenerateSceneColors(int Amount)
        {
            Random r = new Random();
            SceneColors = new Color[Amount];
            var tmp = new HashSet<int>();
            int j = r.Next(0, PossibleColors.Length - 1);
            int i = 0;
            while(i < SceneColors.Length)
            {
                if (tmp.Add(j))
                {
                    SceneColors[i] = PossibleColors[j];
                    i++;
                }
                j = r.Next(0, PossibleColors.Length - 1);
            }
        }
        public void reset()
        {
            for(int i = 0; i < TubesAmount; i++)
            {
                Tubes[i].reset();
            }
            points = 1000;
            time = 0;
        }
        public void GenerateLevel(int Difficulty)
        {
            Random Random = new Random();
            switch (Difficulty)
            {
                case 1:
                    this.TubeSize = 4;
                    this.TubesAmount = Random.Next(5, 6);
                    break;
                case 2:
                    this.TubeSize = 4;
                    this.TubesAmount = Random.Next(6, 9);
                    break;
                case 3:
                case 4:
                    this.TubeSize = 5;
                    this.TubesAmount = Random.Next(9, 13);
                    break;
                default:
                    return;                 
            }
            float XStart = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width / 2 - (TubesAmount*70) / 2;
            float YStart = (float)System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height / 3;
            int empties = TubesAmount / 5 + 1;
            Tubes = new Tube[TubesAmount];
            GenerateSceneColors(TubesAmount - empties);
            Color[] tmp = new Color[TubeSize];
            for(int i = 0; i < TubeSize; i++)
            {
                tmp[i] = Color.Transparent;
            }
            for (int i = 0; i < empties; i++)
            {
                Tubes[i] = new Tube(i * 70 + XStart, YStart, Color.White, 50, 40*TubeSize, tmp, TubeSize);
            }
            int[] ColorUse = new int[SceneColors.Length];
            int r = Random.Next(0, SceneColors.Length);
            for (int i = empties; i < TubesAmount; i++)
            {
                for (int j = 0; j < TubeSize;)
                {
                    while(ColorUse[r] >= TubeSize)
                    {
                        r = Random.Next(0, SceneColors.Length);
                    }
                    tmp[j] = SceneColors[r];
                    ColorUse[r]++;
                    j++;
                    r = Random.Next(0, SceneColors.Length);
                }
                Tubes[i] = new Tube(i * 70 + XStart, YStart, Color.White, 50, 40*TubeSize, tmp, TubeSize);
            }
        }
        public bool IsSolved()
        {
            int tmp = 0;
            for (int i = 0; i < Tubes.Length; i++)
            {
                if (Tubes[i].IsHomo())
                {
                    tmp++;
                }
            }
            if (tmp == SceneColors.Length)
            {
                return true;
            }
            return false;
        }
        public void MoveTubes(int fromIndex, int toIndex)
        {
            if (fromIndex != toIndex && (!Tubes[fromIndex].Colors[0].Equals(Color.Transparent) || Tubes[toIndex].Colors[Tubes[toIndex].TubeSize - 1].Equals(Color.Transparent)) && !Tubes[fromIndex].IsHomo())
            {
                int colorFromIndex = -1;
                for (int i = Tubes[fromIndex].TubeSize - 1; i > -1; i--)
                {
                    if (!Tubes[fromIndex].Colors[i].Equals(Color.Transparent))
                    {
                        colorFromIndex = i;
                        break;
                    }
                }
                if (colorFromIndex == -1)
                {
                    return;
                }
                int colorToIndex = -1;
                if (Tubes[toIndex].Colors[0].Equals(Color.Transparent))
                {
                    colorToIndex = 0;
                }
                else
                {
                    for (int i = 1; i < Tubes[toIndex].TubeSize; i++)
                    {
                        if (Tubes[toIndex].Colors[i].Equals(Color.Transparent))
                        {
                            if (Tubes[toIndex].Colors[i - 1].Equals(Tubes[fromIndex].Colors[colorFromIndex]))
                            {
                                colorToIndex = i;
                            }
                            break;
                        }
                    }
                }
                if (colorToIndex == -1)
                {
                    return;
                }
                bool iterate = false;

                if (colorFromIndex > 0)
                {
                    iterate = Tubes[fromIndex].Colors[colorFromIndex].Equals(Tubes[fromIndex].Colors[colorFromIndex - 1]);
                }

                Tubes[toIndex].Colors[colorToIndex] = Tubes[fromIndex].Colors[colorFromIndex];
                Tubes[fromIndex].Colors[colorFromIndex] = Color.Transparent;

                if (iterate)
                {
                    MoveTubes(fromIndex, toIndex);
                    points += 50;
                }
                points -= 50;
                if (Tubes[toIndex].IsHomo())
                {
                    points += 500;
                }
            }
        }
        public void tick()
        {
            time++;
            points -= 10;
        }
        public void Draw(Graphics g)
        {
            for(int i = 0; i < Tubes.Length; i++)
            {
                Tubes[i].Draw(g);
            }
        }
    }
}
