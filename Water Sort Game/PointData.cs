﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace Water_Sort_Game
{
    [Serializable]
    public class PointData : ISerializable, IComparable<PointData>
    {
        public int points { get; set; }
        public string name { get; set; }
        public int gamemode { get; set; }
        public PointData(int points, string name, int gamemode)
        {
            this.points = points;
            this.name = name;
            this.gamemode = gamemode;
        }
        public PointData(SerializationInfo info, StreamingContext ctxt)
        {
            this.points = info.GetInt32("points");
            this.gamemode = info.GetInt32("gamemode");
            this.name = info.GetString("name");
        }
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("points", points);
            info.AddValue("name", name);
            info.AddValue("gamemode", gamemode);
        }
        public void Save()
        {
            if (this.name.Trim().Length != 0)
            {
                List<PointData> result = null;
                Stream str = null;
                BinaryFormatter bf = new BinaryFormatter();

                if (File.Exists("leaderboard.bin"))
                {
                    str = File.OpenRead("leaderboard.bin");
                    result = (List<PointData>)bf.Deserialize(str);
                    str.Close();
                }
                else
                {
                    result = new List<PointData>();
                }
                bool updated = false;
                foreach (PointData s in result)
                {
                    if (this.name.Equals(s.name) && this.gamemode == s.gamemode && this.points > s.points)
                    {
                        s.points = this.points;
                        updated = true;
                        break;
                    }
                }
                if (!updated)
                {
                    result.Add(this);
                }
                str = File.Create("leaderboard.bin");
                bf.Context = new StreamingContext(StreamingContextStates.CrossAppDomain);
                bf.Serialize(str, result);
                str.Close();
            }
        }
        public static List<PointData> Load()
        {
            List<PointData> result = null;
            if (File.Exists("leaderboard.bin"))
            {
                Stream str = File.OpenRead("leaderboard.bin");
                BinaryFormatter bf = new BinaryFormatter();
                result = (List<PointData>)bf.Deserialize(str);
                str.Close();
            }
            return result;
        }
        public override string ToString()
        {
            return String.Format("{0}\t{1}", name, points);
        }

        public int CompareTo(PointData other)
        {
            if(other == null)
            {
                return 1;
            } else
            {
                return other.points.CompareTo(this.points);
            }
        }
    }
}
